from rest_framework import permissions

class IsGestion(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.groups.get().name=='recepcionista'
    
class IsPedidos(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.groups.get().name=='recepcionista'
    
class IsCocina(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.groups.get().name=='recepcionista'