from rest_framework import routers
from .views import ProductosViewSet,PedidosViewSet,UsuarioViewSet

router=routers.DefaultRouter()
router.register(r'productos',ProductosViewSet, basename='productos')
router.register(r'pedidos',PedidosViewSet, basename='pedidos')
router.register(r'usuarios',UsuarioViewSet, basename='usuarios')

urlpatterns=router.urls