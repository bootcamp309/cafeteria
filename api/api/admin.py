from django.contrib import admin
from .models import Producto,Pedido,Estado,Usuario
# Register your models here.

admin.site.register(Producto)
admin.site.register(Pedido)
admin.site.register(Estado)
admin.site.register(Usuario)