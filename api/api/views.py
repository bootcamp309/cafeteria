import json
from venv import logger
from django.forms import ValidationError
from django.http import Http404
from rest_framework import viewsets,status
from rest_framework.decorators import action
from rest_framework.response import Response
from .models import Producto,Pedido,Usuario
from .serializer import ProductoSerializer,PedidoSerializer,UsuarioSerializer
from .permissions import IsGestion,IsCocina,IsPedidos
from rest_framework.permissions import DjangoModelPermissions
from django_filters.rest_framework import DjangoFilterBackend

class ProductosViewSet(viewsets.ModelViewSet):
    serializer_class = ProductoSerializer
    queryset = Producto.objects.all()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response({"status": "ok", "message": "Producto añadido correctamente"})     
    
    def destroy(self, request, pk=None):
        try:
            instance = self.get_object()
            self.perform_destroy(instance)
        except Http404:
            pass
        return Response({"status": "ok", "message": "Producto eliminado correctamente"})     
    def get_permissions(self):
        permission_classes=[DjangoModelPermissions]
        return [permission() for permission in permission_classes]
    




class PedidosViewSet(viewsets.ModelViewSet):
    serializer_class = PedidoSerializer
    queryset = Pedido.objects.all()
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['estado']
    
    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        try:
            self.perform_create(serializer)
            headers = self.get_success_headers(serializer.data)
        except Http404:
            return Response({"status": "ok", "message": Http404})     
        return Response({"status": "ok", "message": "Pedido añadido correctamente"})     
    
    def get_permissions(self):
        permission_classes=[DjangoModelPermissions]
        return [permission() for permission in permission_classes]
    def destroy(self, request, pk=None):
        try:
            instance = self.get_object()
            self.perform_destroy(instance)
        except Http404:
            return Response({"status": "ok", "message": Http404})     
        return Response({"status": "ok", "message": "Pedido eliminado correctamente"})     

    

    @action(detail=True,methods=["get"])
    def productos(self,request,pk=None):
        id_pedido=self.kwargs['pk']
        try:
            pedido=Pedido.objects.get(pk=id_pedido)
            lista_productos=[]
            for producto_id in pedido.lista_productos:
                lista_productos.append(Producto.objects.get(pk=producto_id))
            return Response(ProductoSerializer(lista_productos,many=True).data,
                    status=status.HTTP_200_OK)
        except Exception as e:
            print(e)
            return Response(None,status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    
    @action(detail=True,methods=["get"])
    def total(self,request,pk=None):
        id_pedido=self.kwargs['pk']
        try:
            pedido=Pedido.objects.get(pk=id_pedido)
            total=0
            for producto in pedido.lista_productos:
                parcial=producto["total"]
                total=total+parcial
            employee_string = '{"total": '+str(total)+'}'
            json_object = json.loads(employee_string)
            return Response(json_object,status=status.HTTP_200_OK)
        except Exception as e:
            print(e)
            return Response(None,status=status.HTTP_500_INTERNAL_SERVER_ERROR)




class UsuarioViewSet(viewsets.ModelViewSet):
    serializer_class = UsuarioSerializer
    queryset = Usuario.objects.all()

    @action(detail=True,methods=["get"])
    def grupo(self,request,pk=None):
        id_usuario=self.kwargs['pk']
        try:
            usuario=Usuario.objects.get(pk=id_usuario)
            valor=usuario.groups.values_list('name',flat = True)
            return Response(valor,status=status.HTTP_200_OK)
        except Exception as e:
            print(e)
            return Response(None,status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def destroy(self, request, pk=None):
        try:
            instance = self.get_object()
            self.perform_destroy(instance)
        except Http404:
            return Response({"status": "ok", "message": Http404})     
        return Response({"status": "ok", "message": "Usuario eliminado correctamente"})     

    
