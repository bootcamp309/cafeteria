from rest_framework import serializers
from .models import Producto,Pedido,Usuario
from django.contrib.auth.hashers import make_password

import json

class ProductoSerializer(serializers.ModelSerializer):
    class Meta:
        model=Producto
        fields=["id","descripcion","precio"]

class PedidoSerializer(serializers.ModelSerializer):
    class Meta:
        model=Pedido
        fields=["id","nombre_cliente","numero_mesa","lista_productos","total","estado"]
 
    
class UsuarioSerializer(serializers.ModelSerializer):
    class Meta:
        model=Usuario
        fields=["id","username","password"]
    
    def create(self, validated_data):
        validated_data['password'] = make_password(validated_data['password'])
        return super().create(validated_data)
    
    def update(self, instance, validated_data):
         validated_data['password'] = make_password(validated_data['password'])
         super().update(instance=instance, validated_data=validated_data)
         return instance