from django.utils import timezone
from django.db import models
from .managers import UserManager

from django.db import models
from django.contrib.auth.models import AbstractUser
from django.conf import settings


class Producto(models.Model):
    id=models.AutoField(primary_key=True)   
    descripcion = models.CharField(max_length=100)
    precio=models.IntegerField()

    def __str__(self) -> str:
        return self.descripcion
    

class Estado(models.Model):
    id=models.IntegerField(primary_key=True)
    descripcion = models.CharField(max_length=50)
    def __str__(self) -> str:
        return self.descripcion

class Pedido(models.Model):
    id=models.AutoField(primary_key=True)
    numero_mesa=models.IntegerField(blank=False,null=False)
    nombre_cliente = models.CharField(max_length=200)
    lista_productos = models.JSONField()
    fecha_de_creacion = models.DateField(default=timezone.now,blank=True,null=True)
    hora_de_creacion = models.TimeField(default=timezone.now,blank=False,null=False)
    fecha_de_cierre = models.DateField(blank=True,null=True)
    hora_de_cierre = models.TimeField(blank=True,null=True)
    estado = models.CharField(max_length=200)
    total=models.IntegerField()
    
    def __str__(self) -> str:
        return self.nombre_cliente

class Usuario(AbstractUser):
    id=models.AutoField(primary_key=True)
    user_name = models.CharField(('user name'), max_length=35)
    email = models.EmailField(('Dirección de correo'))
    date_joined = models.DateTimeField(('date joined'), auto_now_add=True)
    is_active = models.BooleanField(('active'), default=True)
    is_staff = models.BooleanField(('staff'), default=False)
    is_superuser = models.BooleanField(('superuser'), default=False)
    
    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = []


    class Meta:
        verbose_name = 'Usuario'
        verbose_name_plural = 'Usuarios'

    