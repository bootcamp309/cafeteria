# Cafeteria



## Para el proyecto api ejecutar los comandos en el directorio
python -m virtualenv .venv
.\.venv\Scripts\activate
pip install django
django-admin startproject cafeteria .

docker-compose up -d
pip install -r requirements.txt

python manage.py createsuperuser
python .\manage.py startapp api
python .\manage.py createsuperuser
pip install djangorestframework
python .\manage.py runserver
python .\manage.py makemigrations
python .\manage.py migrate

## Acceso a admin
http://localhost:8000/admin

generar los grupos cocina, administrador, recepcionista y dar permisos


## Iniciar api
python .\manage.py runserver

## Para el proyecto front ejecutar los comandos en el directorio
npm -f install @material-ui/icons
npm -f install @material-ui/core
npm -f install react-router-dom

## Iniciar front
npm run dev

## Acceso a la aplicación a traves de la url
http://localhost:5173/

Los usuarios creados desde el admin con sus respectivas contraseñas podran acceder a la aplicación y tendran los mismos permisos según sus grupos