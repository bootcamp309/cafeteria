import React, { useEffect, useState } from "react";
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import {Link} from "react-router-dom";


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  container: {
    marginTop: theme.spacing(2),
  },
  paper: {
    padding: theme.spacing(2),
    color: theme.palette.text.secondary,
  },
}));

export default function UsuariosList() {
  const classes = useStyles();

  const [usuarios, setUsuarios] = useState([]);
  useEffect(() => {
    UsuariosGet()
  }, [])
  

  
  const UsuariosGet = () => {
    
    fetch('http://localhost:8000/usuarios/', {
      method: 'GET',
      headers: {
          Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
          'Content-Type': 'application/json',
      },
      })
          .then((res) => res.json())
          .then((data) => {
              setUsuarios(data)
          })
  }

  const UsuariosEdit = id => {
    window.location = '/usuariosedit/'+id
  }

  const UsuariosDelete = id => {
    var data = {
      'id': id
    }
    fetch('http://localhost:8000/usuarios/'+id+'/', {
      method: 'DELETE',
      headers: {
        Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    })
    .then(res => res.json())
    .then(
      (result) => {
        alert(result['message'])
        if (result['status'] === 'ok') {
          UsuariosGet();
        }
      }
    )
  }

  return (
    <>
          <div className={classes.root}>
          <Container className={classes.container} maxWidth="lg">    
            <Paper className={classes.paper}>
              <Box flexGrow={1}>
                  <Typography elementt="h2" variant="h6" color="primary" gutterBottom>
                    Usuarios
                  </Typography>
                </Box>
                <Link to="/usuariosadd">
                    <Button variant="contained" color="primary">
                      Añadir usuario
                    </Button>
                </Link>
              <TableContainer elementt={Paper}>
              <Table className={classes.table} aria-label="simple table">
                <TableHead>
                  <TableRow>
                  <TableCell align="center">ID</TableCell>
                    <TableCell align="center">Usuario</TableCell>
                    <TableCell align="center">Acciones</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {usuarios.map((usuario) => (
                    <TableRow key={usuario.id}>
                      <TableCell align="center">{usuario.id}</TableCell>
                      <TableCell align="center">{usuario.username}</TableCell>
                      <TableCell align="center">
                        <ButtonGroup color="primary" aria-label="outlined primary button group">
                          <Button onClick={() => UsuariosEdit(usuario.id)}>Editar</Button>
                          <Button onClick={() => UsuariosDelete(usuario.id)}>Eliminar</Button>
                        </ButtonGroup>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </Paper>
        </Container>
      </div>
     

    </>
 );
}