import React, { useState,useEffect} from "react";
import { useParams } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormPopup from "./DetalleAdd";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', 
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function PedidosEdit() {
  const classes = useStyles();

  const { id } = useParams();
  
  useEffect(() => {
    fetch("http://localhost:8000/pedidos/"+id+"/",
    {method: 'GET',
    headers: {
      Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
      'Content-Type': 'application/json',
    }}
    )
      .then(res => res.json())
      .then((result) => {
        setNombreCliente(result['nombre_cliente'])
        setNumeroMesa(result['numero_mesa'])
        setProductos({"productos":result['lista_productos']})
        setTotalPedido(result["total"])
        setEstado(result["estado"])
     } 
    )
  }, [id])

  const handleSubmitPedido = event => {
    event.preventDefault();
    var data = {
      'nombre_cliente': nombre_cliente,
      'numero_mesa': numero_mesa,
      'lista_productos':JSON.parse(JSON.stringify(productos.productos)),
      'total':total(),
      'estado':estado
    }
    fetch('http://localhost:8000/pedidos/'+id+'/', {
      method: 'PUT',
      headers: {
        Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    })
    .then(res => res.json()).then(
      (result) => {
          window.location.href = '/pedidospendientes';
      }
    )
  }

  function eliminar(id) {
    const listado = productos.productos.filter((producto) => producto.id !== id);
    setProductos({"productos":listado});
  }

  function mas(id) {
    id=Number(id);    
    if(productos && productos.productos){
      let iguales=productos.productos.filter((item) => item.id == id);
      if(iguales.length>0){
        iguales[0].cantidad = iguales[0].cantidad+1;
        iguales[0].total = iguales[0].cantidad*iguales[0].precio;
        setProductos({"productos":productos.productos});        
      }
    }
  }

  function menos(id) {
    id=Number(id);    
    if(productos && productos.productos){
      let iguales=productos.productos.filter((item) => item.id == id);
      if(iguales.length>0){
        if(iguales[0].cantidad>1){
          iguales[0].cantidad = iguales[0].cantidad-1;
          iguales[0].total = iguales[0].cantidad*iguales[0].precio;
          setProductos({"productos":productos.productos});        
        }
      }
    }
  }


  const PedidoDetalle = ({ detalle }) => {
    return (
      
      <Table>
          <TableHead>
            <TableRow>
              <TableCell align="center">Descripcion</TableCell>
              <TableCell align="center">Cantidad</TableCell>
              <TableCell align="center">Precio</TableCell>
              <TableCell align="center">Total</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
          {detalle && detalle.map((producto) => (
                <TableRow key={producto.descripcion}>
                  <TableCell align="center">{producto.descripcion}</TableCell>
                  <TableCell align="center">{producto.cantidad}</TableCell>
                  <TableCell align="center">{producto.precio.toLocaleString('es-PY')}</TableCell>
                  <TableCell align="center">{producto.total.toLocaleString('es-PY')}</TableCell>
                </TableRow>
              ))}
              
          </TableBody>
        </Table>
    );
  };


  const [nombre_cliente, setNombreCliente] = useState('');
  const [numero_mesa, setNumeroMesa] = useState('');
  
  const [productos, setProductos] = useState();
  const [descripcion, setDescripcion] = useState(0);
  const [cantidad, setCantidad] = useState(0);
  const [precio, setPrecio] = useState(0);
  const [totalpedido, setTotalPedido] = useState(0);
  const [estado, setEstado] = useState(0);
  const [showPopup, setShowPopup] = useState(false);
  const [formData, setFormData] = useState({ id:0,descripcion:"",cantidad:0,precio:0,total:0 });
  function añadir_detalle(event,id, descripcion,cantidad,precio) {
    event.preventDefault();
    id=Number(id);
    cantidad=Number(cantidad);
    precio=Number(precio);
    let total=Number(precio)*Number(cantidad);

    setFormData({id, descripcion,cantidad,precio,total});
    
    
    if(productos && productos.productos){
      let diferentes=productos.productos.filter((item) => item.id !== id);
      let iguales=productos.productos.filter((item) => item.id == id);
      if(iguales.length>0){
        iguales[0].cantidad = iguales[0].cantidad+cantidad;
        iguales[0].total = iguales[0].total+total;
        diferentes=diferentes.concat(iguales);
      }else{
        let i = 0;
        let nuevo=[{id, descripcion,cantidad,precio,total}]
        while (i < productos.productos.length) {
          nuevo=nuevo.concat(productos.productos[i]);
          i++;
        }
        setProductos({"productos":nuevo});
      }
    }else{
      setProductos( {"productos":[{id, descripcion,cantidad,precio,total}]});    
    }
    
    


  }
  function total(){
    let variable_total=0;
    if(productos && productos.productos){
      let i = 0;
      while (i < productos.productos.length) {
        variable_total=variable_total+productos.productos[i].total;
        i++;
      }
    }
    return variable_total;
  }

  return (



    <Container maxWidth="xs">
    

    




    
    <div className={classes.paper}>
        <Typography element="h1" variant="h5">
          Pedido
        </Typography>
        <Grid container spacing={2}>
            <Grid item xs={12}>
            <Typography elementt="h1" variant="h7" gutterBottom>
                Nombre del cliente: {nombre_cliente}
              </Typography>
            
            
            </Grid>
            <Grid item xs={12}>
            <Typography elementt="h1" variant="h7" gutterBottom>
                Número de mesa: {numero_mesa}
              </Typography>
            </Grid>
            <Grid>
            
            
      {showPopup && (
          <FormPopup onFormSubmit={añadir_detalle} />
      )}
      
          {productos && <PedidoDetalle detalle={productos.productos}/>}

            </Grid>

          </Grid>
          <br></br>
          <Grid>
          <br></br>
          <Typography elementt="h1" variant="h6" gutterBottom>
                Estado
              </Typography>
                    <br></br>
        <Select
        label="Estado" name="estado" id="estado" onChange={(event) => setEstado(event.target.value)}
        defaultValue="Pendiente"
        sx={{
       marginTop:35,        
          width: 250,
          height: 50, 
        }}
      >
                <MenuItem value="Pendiente">Pendiente</MenuItem>
                <MenuItem value="Finalizado">Finalizado</MenuItem>                  
             
       </Select></Grid>
          <form className={classes.form} onSubmit={handleSubmitPedido}>

          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={handleSubmitPedido}
          >
            Actualizar pedido
          </Button>
          </form>
      
    </div>
    </Container>






  );
}