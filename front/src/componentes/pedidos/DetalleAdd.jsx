import React, { useEffect, useState } from "react";
import Modal from "react-modal";
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import TextField from '@material-ui/core/TextField';
import { Grid } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', 
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));


function FormPopup(props) {
  const classes = useStyles();
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const [cantidad, setCantidad] = useState(1);
  const [descripcion, setDescripcion] = useState("");
  const [producto_seleccionado, setProductoSeleccionado] = useState(null);

  const handleOpenModal = () => {
    setModalIsOpen(true);
  };

  const handleCloseModal = () => {
    setModalIsOpen(false);
  };

  const handleSubmit = (event) => {
    props.onFormSubmit(event,JSON.parse(producto_seleccionado).id, JSON.parse(producto_seleccionado).descripcion, cantidad, JSON.parse(producto_seleccionado).precio);
    handleCloseModal();
  };

  const [productos, setProductos] = useState([]);
  useEffect(() => {
    ProductosGet()
  }, [])
  


  const ProductosGet = () => {
    fetch('http://localhost:8000/productos/', {
      method: 'GET',
      headers: {
          Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
          'Content-Type': 'application/json',
      },
      })
          .then((res) => res.json())
          .then((data) => {
              setProductos(data)
          })
  }

    
 

  return (
      <div>
        <Box flexGrow={1}>
              <Typography elementt="h2" variant="h6" color="primary" gutterBottom>
                Productos
              </Typography>
            </Box>
          
      <button onClick={handleOpenModal}>Agregar productos</button>
      <Modal isOpen={modalIsOpen} onRequestClose={handleCloseModal}>
        
        
          <Box flexGrow={1}>
              <Typography elementt="h2" variant="h6" color="primary" gutterBottom>
                Añadir productos
              </Typography>
            </Box>
    
          <Grid>
        <InputLabel id="demo-simple-select-label">Producto</InputLabel>
        <br></br>
        <Select
        label="Age" name="descripcion" id="descripcion" onChange={(event) => setProductoSeleccionado(event.target.value)}
        defaultValue="Benz"
        placeholder="Enter Car Brand"
        sx={{
       marginTop:35,        
          width: 250,
          height: 50, 
        }}
      >
               {productos.map((producto) => (
                <MenuItem value={JSON.stringify(producto)} >{producto.descripcion}</MenuItem>                  
              ))}
       </Select></Grid><br></br>
          <TextField
                variant="outlined"
                required
                fullWidth
                id="cantidad"
                label="Cantidad"
                type="number"
                onChange={(e) => setCantidad(e.target.value)}
              />
                    <Button
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={handleSubmit}
          >
            Añadir
          </Button>  
          <Button
            fullWidth
            variant="contained"
            color="primary"
            onClick={handleCloseModal}
          >
            Cancelar
          </Button>
        
      </Modal>
      </div>

  );
}

export default FormPopup;