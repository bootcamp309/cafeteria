import React, { useEffect, useReducer, useState } from "react";
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import {Link} from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  container: {
    marginTop: theme.spacing(2),
  },
  paper: {
    padding: theme.spacing(2),
    color: theme.palette.text.secondary,
  },
}));

export default function PedidosPendientesList({ onLogout, userId }) {
  const classes = useStyles();

  const [pedidos, setPedidos] = useState([]);
  
  useEffect(() => {
    PedidosGet()
  }, [])
  





  const PedidosGet = () => {
    fetch('http://127.0.0.1:8000/pedidos/?estado=Pendiente', {
      method: 'GET',
      headers: {
          Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
          'Content-Type': 'application/json',
      },
      })
          .then((res) => res.json())
          .then((data) => {
              setPedidos(data)
          })
  }

  
  const PedidoEdit = id => {
    window.location = '/pedidospendientesedit/'+id
  }

  const PedidosDelete = id => {
    var data = {
      'id': id
    }
    fetch('http://localhost:8000/pedidos/'+id+'/', {
      method: 'DELETE',
      headers: {
        Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    })
    .then(res => res.json())
    .then(
      (result) => {
        alert(result['message'])
        if (result['status'] === 'ok') {
          PedidosGet();
        }
      }
    )
  }



  const PedidoDetalle = ({ detalle }) => {
    return (
      <Table>
          <TableHead>
            <TableRow>
              <TableCell align="center">Descripción</TableCell>
              <TableCell align="center">Cantidad</TableCell>
              <TableCell align="center">Precio</TableCell>
              <TableCell align="center">Total</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
          {detalle.map((producto) => (
                <TableRow key={producto.descripcion}>
                  <TableCell align="center">{producto.descripcion}</TableCell>
                  <TableCell align="center">{producto.cantidad}</TableCell>
                  <TableCell align="center">{producto.precio.toLocaleString('es-PY')}</TableCell>
                  <TableCell align="center">{producto.total.toLocaleString('es-PY')}</TableCell>
                  
                </TableRow>
              ))}
          </TableBody>
        </Table>
    );
  };
  



  return (
    <div className={classes.root}>
      <Container className={classes.container} maxWidth="lg">    
        <Paper className={classes.paper}>
          <Box flexGrow={1}>
              <Typography elementt="h2" variant="h6" color="primary" gutterBottom>
                Pedidos
              </Typography>
            </Box>
            <Link to="/pedidosadd">
                <Button variant="contained" color="primary">
                  Añadir Pedido
                </Button>
            </Link>
          <TableContainer elementt={Paper}>
          <Table className={classes.table} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell align="center">ID</TableCell>
                <TableCell align="center">Nombre Cliente</TableCell>
                <TableCell align="center">Número de mesa</TableCell>
                <TableCell align="center">Productos</TableCell>
                <TableCell align="center">Total</TableCell>
                <TableCell align="center">Estado</TableCell>
                <TableCell align="center">Acciones</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {pedidos.map((pedido) => (
                <TableRow key={pedido.id}>
                  <TableCell align="center">{pedido.id}</TableCell>
                  <TableCell align="center">{pedido.nombre_cliente}</TableCell>
                  <TableCell align="center">{pedido.numero_mesa}</TableCell>
                  <TableCell align="center">{<PedidoDetalle detalle={pedido.lista_productos}/>}</TableCell>
                  <TableCell align="center">{pedido.total.toLocaleString('es-PY')}</TableCell>
                  <TableCell align="center">{pedido.estado}</TableCell>
                  <TableCell align="center">
                    <ButtonGroup color="primary" aria-label="outlined primary button group">
                      <Button onClick={() => PedidoEdit(pedido.id)}>Editar</Button>
                      <Button onClick={() => PedidosDelete(pedido.id)}>Eliminar</Button>
                    </ButtonGroup>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Paper>
    </Container>
  </div>
  );
}