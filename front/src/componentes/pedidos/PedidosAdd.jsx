import React, { useState } from "react";
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import FormPopup from "./DetalleAdd";



const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', 
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));


function PedidosAdd() {
  const classes = useStyles();
  
  const handleSubmitPedido = event => {
    event.preventDefault();
    var data = {
      'nombre_cliente': nombre_cliente,
      'numero_mesa': numero_mesa,
      'lista_productos':JSON.parse(JSON.stringify(productos.productos)),
      'total':total(),
      'estado':"Pendiente"
    }
    fetch('http://localhost:8000/pedidos/', {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    })
    .then(res => res.json()).then(
      (result) => {
        if (result['status'] === 'ok') {
          window.location.href = '/pedidos';
        }
      }
    )
  }

  function eliminar(id) {
    const listado = productos.productos.filter((producto) => producto.id !== id);
    setProductos({"productos":listado});
  }

  const PedidoDetalle = ({ detalle }) => {
    return (
      
      <Table>
          <TableHead>
            <TableRow>
              <TableCell align="center">Descripcion</TableCell>
              <TableCell align="center">Cantidad</TableCell>
              <TableCell align="center">Precio</TableCell>
              <TableCell align="center">Total</TableCell>
              <TableCell align="center"></TableCell>
              <TableCell align="center"></TableCell>
              <TableCell align="center"></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
          {detalle && detalle.map((producto) => (
                <TableRow key={producto.descripcion}>
                  <TableCell align="center">{producto.descripcion}</TableCell>
                  <TableCell align="center">{producto.cantidad}</TableCell>
                  <TableCell align="center">{producto.precio.toLocaleString('es-PY')}</TableCell>
                  <TableCell align="center">{producto.total.toLocaleString('es-PY')}</TableCell>
                  <TableCell align="center"><Button variant="contained" color="primary"type="button" onClick={() => eliminar(producto.id)}>Eliminar</Button></TableCell>
                   <TableCell align="center"><Button variant="contained" color="primary"type="button" onClick={() => mas(producto.id)}>+</Button></TableCell>
                  <TableCell align="center"><Button variant="contained" color="primary"type="button" onClick={() => menos(producto.id)}>-</Button></TableCell>
                </TableRow>
              ))}
              
          </TableBody>
        </Table>
    );
  };



  const [nombre_cliente, setNombreCliente] = useState('');
  const [numero_mesa, setNumeroMesa] = useState('');
  
  const [productos, setProductos] = useState();
  const [descripcion, setDescripcion] = useState(0);
  const [cantidad, setCantidad] = useState(0);
  const [precio, setPrecio] = useState(0);
  const [id, setId] = useState(0);
  const [totalpedido, setTotalPedido] = useState(0);
  const [showPopup, setShowPopup] = useState(false);
  const [formData, setFormData] = useState({ id:0,descripcion:"",cantidad:0,precio:0,total:0 });
  

  function añadir_detalle(event,id, descripcion,cantidad,precio) {
    event.preventDefault();
    id=Number(id);
    cantidad=Number(cantidad);
    precio=Number(precio);
    let total=Number(precio)*Number(cantidad);

    setFormData({id, descripcion,cantidad,precio,total});
    
    
    if(productos && productos.productos){
      let iguales=productos.productos.filter((item) => item.id == id);
      if(iguales.length>0){
        iguales[0].cantidad = iguales[0].cantidad+cantidad;
        iguales[0].total = iguales[0].total+total;
      }else{
        let i = 0;
        let nuevo=[{id, descripcion,cantidad,precio,total}]
        while (i < productos.productos.length) {
          nuevo=nuevo.concat(productos.productos[i]);
          i++;
        }
        setProductos({"productos":nuevo});
      }
    }else{
      setProductos( {"productos":[{id, descripcion,cantidad,precio,total}]});    
    }
    
    


  }

  function total(){
    let variable_total=0;
    if(productos && productos.productos){
      let i = 0;
      while (i < productos.productos.length) {
        variable_total=variable_total+productos.productos[i].total;
        i++;
      }
    }
    return variable_total;
  }

  function mas(id) {
    id=Number(id);    
    if(productos && productos.productos){
      let iguales=productos.productos.filter((item) => item.id == id);
      if(iguales.length>0){
        iguales[0].cantidad = iguales[0].cantidad+1;
        iguales[0].total = iguales[0].cantidad*iguales[0].precio;
        setProductos({"productos":productos.productos});        
      }
    }
  }

  function menos(id) {
    id=Number(id);    
    if(productos && productos.productos){
      let iguales=productos.productos.filter((item) => item.id == id);
      if(iguales.length>0){
        if(iguales[0].cantidad>1){
          iguales[0].cantidad = iguales[0].cantidad-1;
          iguales[0].total = iguales[0].cantidad*iguales[0].precio;
          setProductos({"productos":productos.productos});        
        }
      }
    }
  }

  return (



    <Container maxWidth="xs">
    

    




    
    <div className={classes.paper}>
        <Typography element="h1" variant="h5">
          Pedido
        </Typography>
        
        <Grid container spacing={2}>
            <Typography elementt="h1" variant="h7" color="primary" gutterBottom>
                Nombre del cliente*
              </Typography>
            
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="nombre_cliente"
                onChange={(e) => setNombreCliente(e.target.value)}
              />
            </Grid>
            <Typography elementt="h1" variant="h7" color="primary" gutterBottom>
                Número de mesa*
              </Typography>
          
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                type="number"
                id="numero_mesa"
                label=""
                onChange={(e) => setNumeroMesa(e.target.value)}
              />
            </Grid>
            <Grid>
            <FormPopup onFormSubmit={añadir_detalle} />
      {showPopup && (
          <FormPopup onFormSubmit={añadir_detalle} />
      )}
      
          {productos && <PedidoDetalle detalle={productos.productos}/>}

            </Grid>

            <Grid item xs={12}>
            <Typography element="h1" variant="h6">
              Total del pedido : {total().toLocaleString('es-PY')}
            </Typography>
            </Grid>      
          </Grid>
          <form className={classes.form} onSubmit={handleSubmitPedido}>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={handleSubmitPedido}
          >
            Añadir Pedido
          </Button>
          </form>
      
    </div>
    </Container>






  );
}

export default PedidosAdd;