import React, { useEffect, useState } from "react";
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import {Link} from "react-router-dom";


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  container: {
    marginTop: theme.spacing(2),
  },
  paper: {
    padding: theme.spacing(2),
    color: theme.palette.text.secondary,
  },
}));

export default function ProductosList() {
  const classes = useStyles();

  const [productos, setProductos] = useState([]);
  useEffect(() => {
    ProductosGet()
  }, [])
  

  
  const ProductosGet = () => {
    
    fetch('http://localhost:8000/productos/', {
      method: 'GET',
      headers: {
          Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
          'Content-Type': 'application/json',
      },
      })
          .then((res) => res.json())
          .then((data) => {
              setProductos(data)
          })
  }

  const ProductoEdit = id => {
    window.location = '/productosedit/'+id
  }

  const ProductosDelete = id => {
    var data = {
      'id': id
    }
    fetch('http://localhost:8000/productos/'+id+'/', {
      method: 'DELETE',
      headers: {
        Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    })
    .then(res => res.json())
    .then(
      (result) => {
        alert(result['message'])
        if (result['status'] === 'ok') {
          ProductosGet();
        }
      }
    )
  }

  return (
    <>
          <div className={classes.root}>
          <Container className={classes.container} maxWidth="lg">    
            <Paper className={classes.paper}>
              <Box flexGrow={1}>
                  <Typography elementt="h2" variant="h6" color="primary" gutterBottom>
                    Productos
                  </Typography>
                </Box>
                <Link to="/productosadd">
                    <Button variant="contained" color="primary">
                      Añadir producto
                    </Button>
                </Link>
              <TableContainer elementt={Paper}>
              <Table className={classes.table} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell align="center">ID</TableCell>
                    <TableCell align="center">Descripción</TableCell>
                    <TableCell align="center">Precio</TableCell>
                    <TableCell align="center">Acción</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {productos.map((producto) => (
                    <TableRow key={producto.id}>
                      <TableCell align="center">{producto.id}</TableCell>
                      <TableCell align="center">{producto.descripcion}</TableCell>
                      <TableCell align="center">{producto.precio.toLocaleString('es-PY')}</TableCell>
                      <TableCell align="center">
                        <ButtonGroup color="primary" aria-label="outlined primary button group">
                          <Button onClick={() => ProductoEdit(producto.id)}>Editar</Button>
                          <Button onClick={() => ProductosDelete(producto.id)}>Eliminar</Button>
                        </ButtonGroup>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </Paper>
        </Container>
      </div>
     

    </>
 );
}