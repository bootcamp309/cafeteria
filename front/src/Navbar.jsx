import React from "react";
import { useState, useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { MenuButton } from "@reach/menu-button";
import { Link } from "react-router-dom";
import { Button } from "@material-ui/core";
import Login from "./Login"



const logoutHandler = () => {
  onLogout()
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  navlink:{
    color: 'white',
    textDecoration: 'none'
  }
}));

export default function Navbar({onLogout,userId}) {
  const [user, setUser] = useState()
  const [groups, setGroup] = useState([])

  const recepcionista="recepcionista";
  const cocina="cocina";
  const administrador="administrador";


    useEffect(() => {
      if(groups.length>0){
        console.log(groups);
      }else{
      fetch('http://localhost:8000/usuarios/' + userId + '/grupo/', {
        method: 'GET' /* or POST/PUT/PATCH/DELETE */,
        headers: {
          Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
          'Content-Type': 'application/json',
        },
      })
        .then((res) => res.json())
        .then((groupData) => {
          setGroup(Array.from(groupData))
          window.localStorage.setItem('groups', groups)
        })
      }}
      )
        

  const onLogoutHandler = () => {
    setUserId(null)
    window.localStorage.removeItem('accessToken')
  }

  const logoutHandler = () => {
    onLogout()
  }

  const onLoginHandler = (userId) => {
    console.log(userId)
    setUserId(userId)
  }
  const classes = useStyles();
  return (
    <>
    {userId ? (
    <div className={classes.root}>
 
    <AppBar>
      <Toolbar>
        

  <Button component={Link} variant="contained" color="info" to="/">
  <Typography  className={classes.title}>
            Inicio
          </Typography>
       
  </Button>
  &nbsp;&nbsp;
  {groups.includes(administrador)?(
  <Button component={Link} variant="contained" color="info" to="/productos">
  <Typography  className={classes.title}>
            Productos
          </Typography>
       
  </Button>
  ):(<></>)}
  &nbsp;&nbsp;
  {groups.includes(recepcionista)||groups.includes(administrador)?(
  <Button component={Link} variant="contained" color="info" to="/pedidos">
  <Typography  className={classes.title}>
            Pedidos
          </Typography>
       
  </Button>):(<></>)}
  &nbsp;&nbsp;
  {groups.includes(recepcionista)||groups.includes(cocina)||groups.includes(administrador)?(
  <Button component={Link} variant="contained" color="info" to="/pedidospendientes">
  <Typography  className={classes.title}>
            Pedidos Pendientes
          </Typography>
       
  </Button>):(<></>)}
  &nbsp;&nbsp;
  
  {groups.includes(recepcionista)||groups.includes(administrador)?(
  <Button component={Link} variant="contained" color="info" to="/pedidosfinalizados">
  <Typography  className={classes.title}>
            Pedidos a entregar
          </Typography>
       
  </Button>):(<></>)}
  &nbsp;&nbsp;

  {groups.includes(administrador)?(
  <Button component={Link} variant="contained" color="info" to="/usuarios">
  <Typography  className={classes.title}>
            Usuarios
          </Typography>
       
  </Button>):(<></>)}
  &nbsp;&nbsp;

  <Button variant="contained" color="info" onClick={logoutHandler}>Cerrar Sesión</Button>
  

        </Toolbar>
    </AppBar>
  </div>      
  

    ) : (
      <Login onLogin={onLoginHandler} />
    )}
    </>
  );
}