import { useState } from 'react'
import coffeeIcon from './assets/cafe.png'
import jwtDecode from 'jwt-decode'
import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Input from '@material-ui/core/Input';
import {Link} from "react-router-dom";
import { Grid } from '@material-ui/core';
const Login = ({ onLogin }) => {
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  

  const [error,setError]=useState();
  
  const loginHandle = (e) => {
    e.preventDefault()
    // login and get an user with JWT token
    fetch('http://localhost:8000/api/token/', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        username,
        password,
      }),
    })
      .then((res) => res.json())
      .then((tokenData) => {
        window.localStorage.setItem('accessToken', JSON.stringify(tokenData.access))
        console.log(tokenData);
        setError(tokenData.detail)
        console.log(jwtDecode(tokenData.access).user_id);
        onLogin(jwtDecode(tokenData.access).user_id)
      });
  }

  return (
    <Container maxWidth="xs">
      <div>
            
      
    <form onSubmit={loginHandle}>
      
         
        <Grid container spacing={55}>
        <Grid item xs={12} container justify="center">
        <img src={coffeeIcon} alt="Coffee Icon" width={100} />
        </Grid>
        
        <Grid item xs={12} container justify="center">
        
        <Typography element="h1" variant="h4" fullWidth align="center">
          Sistema de Cafeteria
        </Typography>
        </Grid>
        <br></br>
      
            <Grid item xs={12}>
      <Input 
        aria-label="Usuario"
        fullWidth
        placeholder="Usuario"
        id="username"
        type="text"
        onChange={(e) => {
          setUsername(e.target.value)
        }}
      />
        </Grid> 
        <br></br>
        <Grid item xs={12}>
        <Input
          aria-label="Contraseña"
          placeholder="Contraseña"
          fullWidth
          id="password"
          type="password"
          onChange={(e) => {
            setPassword(e.target.value)
          }}
        />
        </Grid> 
        </Grid> 
        <br></br>
      <Button type="submit" fullWidth variant="contained" color="primary">Iniciar sesión</Button>
    
      <br></br>
      <text>{error}</text>

    </form>
    </div>
    </Container>
  )
}

export default Login
