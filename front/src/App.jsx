import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Navbar from './Navbar'
import ProductosList from "./componentes/productos/ProductosList";
import ProductosAdd from "./componentes/productos/ProductosAdd";
import ProductosEdit from "./componentes/productos/ProductosEdit";
import PedidosList from "./componentes/pedidos/PedidosList";
import PedidosAdd from "./componentes/pedidos/PedidosAdd";
import PedidosEdit from "./componentes/pedidos/PedidosEdit";
import PedidosPendientesList from "./componentes/pedidos/PedidosPendientesList";
import PedidosPendientesEdit from "./componentes/pedidos/PedidosPendientesEdit";
import PedidosFinalizadosList from "./componentes/pedidos/PedidosFinalizadosList";
import PedidosFinalizadosEdit from "./componentes/pedidos/PedidosFinalizadosEdit";
import UsuariosList from "./componentes/usuarios/UsuariosList";
import UsuariosEdit from "./componentes/usuarios/UsuariosEdit";
import UsuariosAdd from "./componentes/usuarios/UsuariosAdd";

import Index from "./Index";
import { useState, useEffect } from 'react'
import Login from './Login'
import jwtDecode from 'jwt-decode'


export default function App() {
  const [userId, setUserId] = useState(null)
  const [groups, setGroup] = useState([])

  const recepcionista="recepcionista";
  const cocina="cocina";
  const administrador="administrador";


  useEffect(() => {
    const token = window.localStorage.getItem('accessToken')
    if (token){
      console.log(token);
      try {
        setUserId(jwtDecode(JSON.parse(token)).user_id)
      }catch (e) {
          console.log(e);
      }    
    }
  }, [])
  
  



  const onLoginHandler = (userId) => {
    console.log(userId)
    setUserId(userId)
    window.location.href = '/';
  }

      useEffect(() => {
      if(userId){
        if(groups.length>0){
          console.log(groups);
        }else{
        fetch('http://localhost:8000/usuarios/' + userId + '/grupo/', {
          method: 'GET' /* or POST/PUT/PATCH/DELETE */,
          headers: {
            Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
            'Content-Type': 'application/json',
          },
        })
          .then((res) => res.json())
          .then((groupData) => {
            setGroup(Array.from(groupData))
            window.localStorage.setItem('groups', groups)
          })
        }}
      }
      )

  const onLogoutHandler = () => {
    setUserId(null)
    setGroup(null)
    window.localStorage.removeItem('accessToken')
  }

  const logoutHandler = () => {
    onLogout()
  }

  return (
    <>
    {userId ? (
      
      
      <Router>
      <div>
        <Navbar  onLogout={onLogoutHandler} userId={userId}/>
        <Routes>
        <Route path='/' element={<Index/>} exact/>
        {groups.includes(administrador)?(

<Route path='/productos' element={<ProductosList/>} exact/>
):(<></>)}
  {groups.includes(administrador)?(




<Route path='/productosadd' element={<ProductosAdd/>} exact/>
):(<></>)}
  {groups.includes(administrador)?(



<Route path='/productosedit/:id' element={<ProductosEdit/>} exact/>
):(<></>)}


{groups.includes(recepcionista)||groups.includes(administrador)?(
<Route path='/pedidos' element={<PedidosList/>} exact/>
):(<></>)}

{groups.includes(recepcionista)||groups.includes(administrador)?(
<Route path='/pedidosadd' element={<PedidosAdd/>} exact/>
):(<></>)}

{groups.includes(recepcionista)||groups.includes(administrador)?(
<Route path='/pedidosedit/:id' element={<PedidosEdit/>} exact/>
):(<></>)}

{groups.includes(recepcionista)||groups.includes(cocina)||groups.includes(administrador)?(
 
<Route path='/pedidospendientes' element={<PedidosPendientesList/>} exact/>
):(<></>)}

{groups.includes(recepcionista)||groups.includes(cocina)||groups.includes(administrador)?(
 
<Route path='/pedidospendientesedit/:id' element={<PedidosPendientesEdit/>} exact/>
):(<></>)}


{groups.includes(recepcionista)||groups.includes(administrador)?(
 
<Route path='/pedidosfinalizados' element={<PedidosFinalizadosList/>} exact/>
):(<></>)}

{groups.includes(recepcionista)||groups.includes(administrador)?(
<Route path='/pedidosfinalizadosedit/:id' element={<PedidosFinalizadosEdit/>} exact/>
):(<></>)}

{groups.includes(administrador)?(
<Route path='/usuarios' element={<UsuariosList/>} exact/>
):(<></>)}

<Route path='/usuariosedit/:id' element={<UsuariosEdit/>} exact/>
{groups.includes(administrador)?(
<Route path='/usuariosadd' element={<UsuariosAdd/>} exact/>
):(<></>)}
        </Routes>
      </div>
    </Router>

    ) : (
      <Login onLogin={onLoginHandler} />
    )}
    </>
    
  );
}
